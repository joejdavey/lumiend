import requests
key_loc = "C:\\Users\Joe\local\TestAPI.txt"
f = open(key_loc, "r")
key = f.read()

base_url = 'http://192.168.1.50/api/' + key +'/'

def get_value(classification, number, key):
    if classification == 'light' or 'l':
        url = base_url + 'lights/' + str(number)
    elif classification == 'group' or 'g':
        url = base_url + 'groups/' + str(number)
    else:
        print('classification not recognised')
    r = requests.get(url).json()
    if key == 'on':
        return r['state']['on']
    elif key == 'bri':
        return r['state']['bri']
    elif key == 'ct':
        return r['state']['ct']
    else:
        print('Key not supported')

def put_action(url, data):
    headers = {"Content-Type": "application/json"}
#    print('data= ' + data)
#    print('url= ' + url)
    r = requests.put(url, data=data, headers=headers)
    return r.content

def build_url(classification, number):
    if classification == 'light' or 'l':
        url = base_url + 'lights/' + str(number) + '/state/'
    elif classification == 'group' or 'g':
        url = base_url + 'groups/' + str(number) + '/action/'
    else:
        print('classification not recognised')
    return url

def state(classification, number, action):
    url = build_url(classification, number)
    if action == 'on':
        data = '{"on":true}'
    elif action == 'off':
        data = '{"on":false}'
    else:
        print('action not recognised')

    r = put_action(url, data)
    return r

def bri(classification, number, value):
    url = build_url(classification, number)
    data = '{"bri":' + value + '}'
    r = put_action(url, data)
    return r

def col(classification, number, x, y, ct):
    url = build_url(classification, number)
    data = '{"xy": [' + str(x) + ', ' + str(y) + '],"ct":' + str(ct) + '}'
#    print(data)
#    print(url)
    r = put_action(url, data)
    return r


def turnOn(light):
    r = state('light', light, 'on')
    return r


def turnOff(light):
    r = state('light', light, 'off')
    return r
