import json
#import secrets
import string


# Configures the pixel library
def setup():
    with open("config.ini", "r") as f:
        config = f.read()
    
    # We do this here instead of globally to potentially allow for different backend on different boards
    import neoPixelBackend

    if "ws2812_rgbw" in config:
        neo = neoPixelBackend.NeoPixelBackend()
        neo.pixelType = "rgbw"
    elif "ws2812_rgb" in config:
        neo = neoPixelBackend.NeoPixelBackend()
        neo.pixelType = "rgb"

# Validate a user ID
def validateUser(username):
    with open('users.dat', 'r') as f:
        currentUsers = json.parse(f.read())
    return username in currentUsers

# Creates a new user. TODO: Authenticate this endpoint somehow.
@app.route('/api', methods=['POST'])
def api_create_user(data):
    '''
    Sample Body: {"devicetype": "my_hue_app#iphone peter"}
    Sample Response: [{"success":{"username": "83b7780291a6ceffbe0bd049104df"}}]
    Sample Stored Data: {"83b7780291a6ceffbe0bd049104df": {"name": "my_hue_app#iphone peter"}}
    '''
    alphabet = string.ascii_letters + string.digits
    username = ''.join(secrets.choice(alphabet) for i in range(20))

    with open('users.dat', 'rw') as f:
        currentUsers = json.parse(f.read())
        currentUsers[username] = {"name": data['devicetype']}
        f.write(currentUsers)

    return json.dumps([{"success": {"username": username}}])

# Get a dump of the device config. TODO: Fill in any important fields
@app.route('/api/<username>/config', methods=['GET'])
def api_config(username):
    if not validateUser(username):
        return False
    
    with open('users.dat', 'r') as f:
        currentUsers = json.parse(f.read())
    
    data = {
        "name": "Lumiend",
        "zigbeechannel": 0,
        "mac": "TODO",
        "dhcp": True,
        "ipaddress": "TODO",
        "netmask": "TODO",
        "gateway": "TODO",
        "proxyaddress": "none",
        "proxyport": 0,
        "UTC": "TODO",
        "localtime": "TODO",
        "timezone": "TODO",
        "whitelist": currentUsers,
        "swversion": "TODO: Is this important?",
        "apiversion": "1.3.0",
        "swupdate": {
            "updatestate": 0,
            "url": "THIS SECTION MAY NEED REPLACING WITH swupdate2",
            "text": "",
            "notify": False
        },
        "linkbutton": False,
        "portalservices": False,
        "portalconnection": "connected",
        "portalstate": {
            "signedon": True,
            "incoming": False,
            "outgoing": True,
            "communication": "disconnected"
        }
    }
    return json.dumps(data)

# Modify device config. TODO: Implement as necessary
@app.route('/api/<username>/config', methods=['PUT'])
def api_modify_config(username, data):
    '''
    Sample Body: {"name":"My bridge"}
    Sample Response: [{"success":{"/config/name":"My bridge"}}]
    '''

    if not validateUser(username):
        return False

    return json.dumps([{"success": {"thisIsADummyFunction": "SoNothingChanged"}}])

# Delete a user account.
@app.route('/api/<username>/config/whitelist/<user>', methods=['DELETE'])
def api_delete_user(username, user):
    '''
    Sample Response: [{"success": "/config/whitelist/1234567890 deleted."}]
    '''
    if not validateUser(username):
        return False

    with open('users.dat', 'rw') as f:
        currentUsers = json.parse(f.read())
        del currentUsers[user]
        f.write(currentUsers)

    return json.dumps([{"success": "/config/whitelist/{} deleted.".format(user)}])

# Get full state. TODO: Implement sections
@app.route('/api/<username>', methods=['GET'])
def api_delete_user(username):
    if not validateUser(username):
        return False
    
    data = {
        "lights": {},
        "groups": {},
        "config": json.parse(api_config(username)),
        "schedules": {}
    }

    return json.dumps(data)


def main():
    setup()
    startFlask()
    if iAmRaspberry():
        import raspberryLights as l
    else:
        import arduinoLights as l

if __name__ == "__main__":
    main()